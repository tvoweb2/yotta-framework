# -*- coding: utf-8 -*-

module Yotta
	class Factory
		def initialize type
			@type = type
		end
	end

	module Protocol
		class Factory < ::Yotta::Factory
			def create opts=nil
				case @type
				when "ssh"
					SSH.new opts
				when "scp"
					SCP.new opts
				when "sftp"
					SFTP.new opts
				end
			end
		end

		class SSH
			require "net/ssh"
			
			def initialize opts
				@host = !opts[:host].nil? ? opts[:host]:nil
				@port = !opts[:port].nil? ? opts[:port]:nil
				@username = !opts[:username].nil? ? opts[:username]:nil
				@password = !opts[:password].nil? ? opts[:password]:nil
				@keys = !opts[:keys].nil? ? opts[:keys]:nil
				@passphrase = !opts[:passphrase].nil? ? opts[:passphrase]:nil
			end

			def deploy src, dest
				raise "SCP::deploy : no src." if src.nil?
				raise "SCP::deploy : no dest." if dest.nil?
				if src.instance_of?(Array) && dest.instance_of?(Array)
					connect {|ssh|
						for i in 0...src.length do
							if !src[i].nil? && !dest[i].nil?
								upload ssh, src[i], dest[i]
							end
						end
					}
				else
					connect {|ssh|
						upload ssh, src, dest
					}
				end
			end

			protected

			def connect
				raise "SCP::deploy : no @host." if @host.nil?
				raise "SCP::deploy : no @username." if @username.nil?
				opt = {
					:port => @port
				}
				opt[:keys] = @keys unless @keys.nil?
				opt[:passphrase] = @passphrase unless @passphrase.nil?
				opt[:password] = @password unless @password.nil?
				::Net::SSH.start(@host, @username, opt) do |ssh|
					yield ssh
				end
			end

			def upload ssh, src, dest
				destdir = ::File.dirname dest
				if !ssh.nil? && dest != ""
					ssh.exec! "mkdir -p #{destdir}"
				end
			end
		end

		class SCP < SSH
			require "net/scp"

			protected

			def upload ssh, src, dest
				if !ssh.nil? && dest != ""
					super ssh, src, dest
					ssh.scp.upload! src, dest
				end
			end
		end

		class SFTP < SSH
			require "net/sftp"

			protected

			def upload ssh, src, dest
				if !ssh.nil? && dest != ""
					super ssh, src, dest
					ssh.sftp.upload! src, dest
				end
			end
		end
	end

	module API
		class BrightcoveMedia
			require "brightcove-api"

			def initialize opts
				read_api_url = opts[:read_api_url] || "https://api.brightcove.com/services/library"
				@api = ::Brightcove::API.new(opts[:read_api_token], read_api_url)
				@publisher_id = opts[:publisher_id]
			end

			def search opts
				fetch "search_videos", opts
			end

			private

			def fetch cmd, opts
				opts[:page_size] = 100
				pagenum = 0
				items = nil
				res = []
				begin
					opts[:page_number] = pagenum
					response = @api.get cmd, opts
					items = response.parsed_response["items"]
					if !items.nil?
						res.concat items
					end
					pagenum += 1
				end while !items.nil? && items.size > 0
				return res
			end
		end

		class EquipmediaMedia
			require "base64"
			require "uri"
			require "open-uri"
			require "json"

			def initialize opts
				@read_api_url = opts[:read_api_url] || "https://api01-platform.stream.co.jp/apiservice/"
				@read_api_token = Base64.strict_encode64(opts[:read_api_token])
				@contract_id = opts[:contract_id]
			end

			def search opts
				if !opts[:mid].nil?
					cmd = "getMediaById"
				elsif !opts[:api_keywords].nil?
					cmd = "getMediaByTag"
				else
					cmd = "getAllMedia"
				end
				fetch cmd, opts
			end

			private

			def fetch cmd, opts
				opts[:token] = @read_api_token
				get cmd, opts
			end
			
			def get cmd, opts
				raise "no api_token" if @read_api_token.nil?
				raise "no api_url" if @read_api_url.nil?
				raise "api_url isnot url" unless @read_api_url.match(/^https?:\/\//)
				query = opts.map{ |k,v| "#{k}=#{v}" }.join("&")
				query = URI.escape(query)
				uri = URI.parse("#{@read_api_url}#{cmd}")
				res = open("#{uri}?#{query}")
				body = res.read
				if m = body.match(/^\w+\((.+)\);?$/)
					json = JSON.parse(m[1])
					json["meta"]
				else
					nil
				end
			end
		end
	end

	module FFMPEG
		require "streamio-ffmpeg"
		require "open3"

		def self.cmd cmd
			out, err, stat = Open3.capture3 cmd
			::FFMPEG.logger.info out
			::FFMPEG.logger.error err if err != ""
			return stat.success?
		end

		class FeaturephoneMovie < ::FFMPEG::Movie
			PROFILE_DOCOMO_HIGH = "docomo_high"
			PROFILE_DOCOMO_LOW  = "docomo_low"
			PROFILE_AU_DEFAULT = "au_default"
			PROFILE_SOFTBANK_DEFAULT = "softbank_default"

			def process path, profile
				case
				# docomo(low)
				when profile === PROFILE_DOCOMO_LOW
					opts = '-y -async 1 -vcodec mpeg4 -s 176x144 -vf "pad=0:in_w/11*9:0:(in_w/11*9-in_h)/2" -r 14.985 -b:v 64k -acodec libfdk_aac -ac 1 -ar 8000 -b:a 16k -f 3gp'
					self.class.split(path, 280).each do |file|
						self.class.streamize file, "mmp4:1"
					end if transcode path, opts
				# docomo(high)
				when profile === PROFILE_DOCOMO_HIGH
					opts = '-y -async 1 -vcodec mpeg4 -s 176x144 -vf "pad=0:in_w/11*9:0:(in_w/11*9-in_h)/2" -r 14.985 -b:v 190k -acodec libfdk_aac -ac 2 -ar 16000 -b:a 64k -f 3gp'
					self.class.split(path, 1980).each do |file|
						self.class.streamize file, "mmp4:1"
					end if transcode path, opts
				# au
				when profile === PROFILE_AU_DEFAULT
					opts = '-y -async 1 -vcodec mpeg4 -s 176x144 -vf "pad=0:in_w/11*9:0:(in_w/11*9-in_h)/2" -r 14.985 -b:v 64k -acodec libfdk_aac -ac 2 -ar 22050 -b:a 64k -f 3g2'
					self.class.split(path, 1480).each do |file|
						self.class.streamize file, "kddi:1", "3g2a"
					end if transcode path, opts
				# SoftBank
				when profile === PROFILE_SOFTBANK_DEFAULT
					opts = '-y -async 1 -vcodec mpeg4 -s 176x144 -vf "pad=0:in_w/11*9:0:(in_w/11*9-in_h)/2" -r 14.985 -b:v 64k -acodec libopencore_amrnb -ac 1 -ar 8000 -b:a 12.2k -f 3gp'
					self.class.split(path, 280).each do |file|
						self.class.streamize file, "3gp5", "3gp4"
					end if transcode path, opts
				end
			end

			def self.split path, kbytes
				FFMPEG.cmd "MP4Box -splits #{kbytes} #{path}"
				curdir = ::File.dirname path
				ext = ::File.extname path
				base = ::File.basename(path).gsub(Regexp.new(Regexp.escape(ext)+"$"),"")
				files = ::Dir.glob("#{curdir}/#{base}_[0-9][0-9][0-9]#{ext}")
				if files.length > 0
					::File.unlink path
					return files
				else
					return [path]
				end
			end

			def self.streamize path, brand=nil, altbrand=nil
				newpath = "#{path}_tmp.mp4"
				cmd = "MP4Box -add #{path}"
				cmd << " -brand #{brand}" if !brand.nil?
				cmd << " -ab #{altbrand}" if !altbrand.nil?
				cmd << " -new #{newpath}"
				FFMPEG.cmd(cmd)
				if ::File.exists? newpath
					::File.unlink path
					::File.rename newpath, path
				end
				return path
			end
		end

		class HLSMovie < ::FFMPEG::Movie
			def self.fetch uri, dst
				ext = ::File.extname(::URI.parse(uri).path)
				if ext === ".m3u8"
					FFMPEG.cmd "ffmpeg -i \"#{uri}\" -acodec copy -vcodec copy -y -bsf:a aac_adtstoasc -f mp4 #{dst}"
				end
			end
		end
	end
end
